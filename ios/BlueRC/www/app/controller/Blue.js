Ext.define('BlueRC.controller.Blue', {
    testValue:1000,
    fingerIsDown:false,
    watchId:null,
    lastAcceleration:null,
    touchStartAcceleration:null,
    touchStartEvent:null,
    lastTouchEvent:null,
    extend:'Ext.app.Controller',
    currentLed:0,
    currentTest:0,
    currentPos:1000,
    isForward:true,
    speed:5,
    blink:false,
    blinkCount:0,
    currentColors:[
        {r:255, g:255, b:255},
        {r:255, g:255, b:255},
        {r:255, g:255, b:255},
        {r:255, g:255, b:255},
        {r:255, g:255, b:255}
    ],
    config:{
        refs:{
            main:'main',
            joystick1:'main #joystick1',
            led1:'main #led1',
            prefs1:'main #prefs1'
            
        },        
        control:{
           main:{
             show:'initializeGUI'
           }
        }
    },
    
    init:function()
    {
        var options = { frequency:40 };
        if(navigator.accelerometer)
            watchId = navigator.accelerometer.watchAcceleration(this.onAcceleration.bind(this), null, options);
    },

    initializeGUI:function()
    {
        var js1 = this.getJoystick1();
        var led1 = this.getLed1();
        var prefs1 = this.getPrefs1();
        
        js1.element.on(['touchstart', 'touchend', 'touchmove'],
            'onJoystickEvent', this);
        
        led1.on(['colorPicked'], 'onColorPicked', this);
        led1.on(['buttonUpdate'], 'onButtonUpdate', this);

        prefs1.on(['testUpdate'], 'onTestUpdate', this);
        
        setInterval(this.onBlink.bind(this), 250);
        setInterval(this.onTest.bind(this), 33);
    },
    
    testTap:function(){
        this.testValue += 100;
        
        if(this.testValue > 2000)
            this.testValue = 1000;
        
        BluetoothServoController.SendServoControl("4:0:" + Math.round(this.testValue / 4.8828173828125));
    },
    
    onJoystickEvent:function (e, target, options, eventController){
        if(eventController.info.eventName == "touchstart")
        {
            this.fingerIsDown = true;
            this.touchStartAcceleration = this.lastAcceleration;
            this.touchStartEvent = e;
            this.lastTouchEvent = e;
        }

        if (eventController.info.eventName == "touchend") {
            this.fingerIsDown = false;
            //4.8828173828125 = 50 hz in uS
            BluetoothServoController.SendServoControl("1:0:" + Math.round(1500 / 4.8828173828125));
            BluetoothServoController.SendServoControl("2:0:" + Math.round(1400 / 4.8828173828125));
        }

        if (eventController.info.eventName == "touchmove") {
           this.lastTouchEvent = e;
        }
        
    },
    
    onColorPicked:function(led, rgb){
        //console.log(rgb);
        
        //4.8828173828125 = 50 hz in uS
        var red = Math.round(4095 - (rgb.r / 255.1) * 4095) ;
        var green = Math.round(4095 - (rgb.g / 255.1) * 4095);
        var blue = Math.round(4095 - (rgb.b / 255.1) * 4095);

        rgb = {r:red, g:green, b:blue};


        switch (this.currentLed)
        {
            
            case 0:
            {
                this.currentColors[0] = rgb;
                this.currentColors[1] = rgb;
                this.currentColors[2] = rgb;
                this.currentColors[3] = rgb;
                
                BluetoothServoController.SendServoControl("1:0:" + red);
                BluetoothServoController.SendServoControl("2:0:" + green);
                BluetoothServoController.SendServoControl("3:0:" + blue);

                BluetoothServoController.SendServoControl("6:0:" + red);
                BluetoothServoController.SendServoControl("7:0:" + green);
                BluetoothServoController.SendServoControl("8:0:" + blue);

                BluetoothServoController.SendServoControl("9:0:" + red);
                BluetoothServoController.SendServoControl("10:0:" + green);
                BluetoothServoController.SendServoControl("11:0:" + blue);

                BluetoothServoController.SendServoControl("14:0:" + red);
                BluetoothServoController.SendServoControl("15:0:" + green);
                BluetoothServoController.SendServoControl("16:0:" + blue);
                break;
            }
            case 1:
            {
                this.currentColors[0] = rgb;
                BluetoothServoController.SendServoControl("1:0:" + red);
                BluetoothServoController.SendServoControl("2:0:" + green);
                BluetoothServoController.SendServoControl("3:0:" + blue);
                break;
            }
            case 2:
            {
                this.currentColors[1] = rgb;
                BluetoothServoController.SendServoControl("6:0:" + red);
                BluetoothServoController.SendServoControl("7:0:" + green);
                BluetoothServoController.SendServoControl("8:0:" + blue);
                break;
            }
            case 3:
            {
                this.currentColors[2] = rgb;
                BluetoothServoController.SendServoControl("9:0:" + red);
                BluetoothServoController.SendServoControl("10:0:" + green);
                BluetoothServoController.SendServoControl("11:0:" + blue);
                break;
            }
            case 4:
            {
                this.currentColors[3] = rgb;
                BluetoothServoController.SendServoControl("14:0:" + red);
                BluetoothServoController.SendServoControl("15:0:" + green);
                BluetoothServoController.SendServoControl("16:0:" + blue);
                break;
            }
            
        }
        
    },
    
    onButtonUpdate:function(index)
    {
        console.log("Button Update...");
        this.currentLed = index;
        if(index != 5)
            this.blink = false;
        else
            this.blink = true;
    },

    onTestUpdate: function (index) {
        console.log("Test Update...");
        
        this.currentTest = index;
    },
    
    
    
    
    onAcceleration:function(acceleration)
    {
        this.lastAcceleration = acceleration;
        
        if(!this.fingerIsDown)
        {
            return;        
        }
        
        var diffAccelY = Math.round(1400 + (100 * (this.touchStartAcceleration.y - acceleration.y)));
        var diffPageX = Math.round(1500 + (15 * (this.touchStartAcceleration.x - acceleration.x)));
        //var diffPageX = Math.round(1500 + (-1 * (this.touchStartEvent.pageX - this.lastTouchEvent.pageX)));
        
        if(diffAccelY > 2000)
            diffAccelY = 2000;
        else if(diffAccelY < 1000)
            diffAccelY = 1000;

        if (diffPageX > 2000)
            diffPageX = 2000;
        else if (diffPageX < 1000)
            diffPageX = 1000;
        
        //left is 2000
        
        //console.log("AY = " + diffAccelY);
        //console.log("PX = "+ diffPageX);


        //BluetoothServoController.SendServoControl("1:0:" + Math.round(diffPageX / 4.8828173828125));
        //BluetoothServoController.SendServoControl("2:0:" + Math.round(diffAccelY / 4.8828173828125));
        
        diffPageX -= 1000;
        diffAccelY -= 1000;
        
        diffPageX = ((diffPageX / 1000) * 205) + 25;
        diffAccelY = ((diffAccelY / 1000) * 205) + 25;
        
        BluetoothServoController.SendFastServoControl("1:" + Math.round(diffPageX));
        BluetoothServoController.SendFastServoControl("2:" + Math.round(diffAccelY));

    },
    
    onBlink:function()
    {
        if(!this.blink)
            return;
        
        var index = this.blinkCount % 4;
        this.blinkCount++;
        
        rgb1 = this.currentColors[index];
        rgb2 = this.currentColors[(index + 1) % 4];
        rgb3 = this.currentColors[(index + 2) % 4];
        rgb4 = this.currentColors[(index + 3) % 4];
        
        BluetoothServoController.SendServoControl("1:0:" + rgb1.r);
        BluetoothServoController.SendServoControl("2:0:" + rgb1.g);
        BluetoothServoController.SendServoControl("3:0:" + rgb1.b);

        BluetoothServoController.SendServoControl("6:0:" + rgb2.r);
        BluetoothServoController.SendServoControl("7:0:" + rgb2.g);
        BluetoothServoController.SendServoControl("8:0:" + rgb2.b);

        BluetoothServoController.SendServoControl("9:0:" + rgb3.r);
        BluetoothServoController.SendServoControl("10:0:" + rgb3.g);
        BluetoothServoController.SendServoControl("11:0:" + rgb3.b);

        BluetoothServoController.SendServoControl("14:0:" + rgb4.r);
        BluetoothServoController.SendServoControl("15:0:" + rgb4.g);
        BluetoothServoController.SendServoControl("16:0:" + rgb4.b);
    },
    
    onTest:function()
    {
        
        if(this.currentTest == 0)
            return;


        if (this.speed > 100) 
        {
            this.speed = 10;
            this.currentPos = 1000;
        }
        
        if(this.currentPos > 2000 || this.currentPos < 1000)
        {
            this.speed++;
            this.isForward = !this.isForward;
        }
        
               
        if(this.isForward)
            this.currentPos += this.speed;
        else
            this.currentPos -= this.speed;
     
         
         BluetoothServoController.SendServoControl(this.currentTest + ":0:" + Math.round(this.currentPos / 4.8828173828125));
        
        
        
    }
    
});