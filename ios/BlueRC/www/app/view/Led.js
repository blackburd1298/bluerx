Ext.define('BlueRC.view.Led', {
    extend:'Ext.Container',
    xtype:'led',
    isInit:false,
    currentLed:0,
    ledNames:[
        "All LEDs",
        "LED 1",
        "LED 2",
        "LED 3",
        "LED 4",
        "Blink!"
            
    ],

    config:{
        layout:{
            type:'hbox'
        },
        items:[
            {
                xtype:'container',
                layout:{
                    type:'vbox'
                },
                items:[
                    {
                        xtype:'container',
                        margin: 40,
                        html:'<div id="mycolorpicker" class="cp-default"></div>'

                    },
                    {
                        xtype:'button',
                        iconCls:'star',
                        iconMask:true,
                        text:'All LEDs',
                        
                        style:{
                            marginLeft:'40px',
                            marginRight:'40px'
                        },
                        handler:function()
                        {
                            
                            var self = this.up('led');

                            self.currentLed++;
                            
                            if(self.currentLed == 6)
                                self.currentLed = 0;

                            this.setText(self.ledNames[self.currentLed]);

                            self.fireEvent("buttonUpdate", self.currentLed);  
                        }
                        
                    }

                ]
            }
        ]
    },
    listeners:{
        painted:function () {
            var self = this;
            if (!this.isInit) {
                this.isInit = true;
                ColorPicker(document.getElementById('mycolorpicker'), function (hex, hsv, rgb) {
                    self.fireEvent("colorPicked", self.currentLed, rgb);
                });
            }
        }
    }
});