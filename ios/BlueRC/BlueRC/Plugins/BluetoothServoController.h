//
//  BluetoothServoController.h
//  BlueRC
//
//  Created by Jason Peterson on 4/6/13.
//
//

#import <Cordova/CDV.h>
#import "StpBlueRC.h"

@interface BluetoothServoController : CDVPlugin
{
    NSString *_currentCallbackId;
    BOOL _isRegistered;
}

@property(nonatomic, strong) NSString* currentCallbackId;
@property(readwrite) BOOL isRegistered;

- (void) onData:(CDVInvokedUrlCommand*)command;
- (void) sendData:(CDVInvokedUrlCommand*)command;
- (void) getIpAddress:(CDVInvokedUrlCommand*)command;

- (void) initialize;

@end
